# stickersHtml

Update: deprecated, Twitter doesn't response old interface any more.

---

I want to use Twitter in old html style(Twitter Web Client), not the shitty Twitter Web App. 
Someone came up with changing user-agent to Internet Explorer's.
Months ago Twitter started to warn users modifying user-agent to IE that legacy browsers were no longer supported.
However, twitter responses googlebot with exactly old style html.

There's one problem left, the gallery view of tweet photos will load stickerHtml from `https://twitter.com/i/tweet/stickersHtml?id=$(data-item-id)&modal=gallery` the API, which twitter stopped supporting since Octuber 2020.

From the [web archives](https://web.archive.org/web/*/https://twitter.com/i/tweet/stickersHtml*) of Wayback Machine, I extracted 10 `stickersHtml`.

`cat json.json |jq -r .tweet_html >> 1010883806033657856.html`

Probably I can rebuild this API.

Related: [github.com/lvnkae/twitter-filter](https://github.com/lvnkae/twitter-filter/commit/4255b21d99c158d5e6b5177cae61a490dff05841)
